<?php
/**
 * Abstract class for all modules
 *
 * @author Piotr Dziedzic <piotr.dziedzic@idium.no>
 * @since 09-04-14
 */
abstract class ModuleAbstract
{
    /**
     * Parsed options for module
     *
     * @var array
     */
    protected $options;

    /**
     * List of default options for module
     *
     * @var array
     */
    static protected $defaultOptions = array();

    /**
     * Collection of modules objects
     *
     * @var array
     */
    static private $instances = array();


    /**
     * Loads module
     *
     * @param	array	$options	List of params passed to module
     *
     * @return	ModuleAbstract
     */
    static public function load($options = array())
    {
        $class = get_called_class();

        if(empty(self::$instances[$class]))
        {
            if(!is_array($options) || !is_array($class::$defaultOptions))
            {
                throw new InvalidArgumentException('Module options have to be an Array.');
            }

            self::$instances[$class] = new $class($options);
        }

        return self::$instances[$class];
    }

    /**
     * Parses options array for module
     *
     * @param	array	$options    List of options
     *
     * @return	array
     */
    abstract protected function parseOptions($options);

    /**
     * Protected constructor
     *
     * @param	array	$options	List of params for initializing module
     */
    protected function __construct($options = array())
    {
        $this->options = $this->parseOptions($options);
    }
}